require('rose-pine').setup({
	--- @usage 'auto'|'main'|'moon'|'dawn'
	variant = 'auto'
})
vim.cmd('colorscheme rose-pine')
