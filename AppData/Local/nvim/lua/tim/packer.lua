-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

require('rose-pine').setup({
	--- @usage 'auto'|'main'|'moon'|'dawn'
	variant = 'moon'
})

return require('packer').startup(function(use)
	use 'wbthomason/packer.nvim'

	-- navigation

	use {
		'nvim-telescope/telescope.nvim', tag = '0.1.2',
		-- or                            , branch = '0.1.x',
		requires = { {'nvim-lua/plenary.nvim'} }
	}

	use('ThePrimeagen/harpoon')

	-- editing

	use('mbbill/undotree')

	use('preservim/vim-markdown')

	-- coding tools

	-- use('nvim-treesitter/nvim-treesitter', {run = ':TSUpdate'})

	use('tpope/vim-fugitive')
	use('airblade/vim-gitgutter')

	use {
		'VonHeikemen/lsp-zero.nvim',
		branch = 'v2.x',
		requires = {
			-- LSP Support
			{'neovim/nvim-lspconfig'},             -- Required
			{'williamboman/mason.nvim'},           -- Optional
			{'williamboman/mason-lspconfig.nvim'}, -- Optional

			-- Autocompletion
			{'hrsh7th/nvim-cmp'},     -- Required
			{'hrsh7th/cmp-nvim-lsp'}, -- Required
			{'L3MON4D3/LuaSnip'},     -- Required
		}
	}

--[[	use { -- debugger
		"puremourning/vimspector",
		cmd = { "VimspectorInstall", "VimspectorUpdate" },
		fn = { "vimspector#Launch()", "vimspector#ToggleBreakpoint", "vimspector#Continue" },
		config = function()
			require("config.vimspector").setup()
		end,
	}]]

	-- appearance
	use({ 'rose-pine/neovim', as = 'rose-pine' })

	use('junegunn/goyo.vim')  -- focused editing
end)

