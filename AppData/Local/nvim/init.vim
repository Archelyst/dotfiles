lua require("tim")

set clipboard+=unnamed

" only wait 100 ms to update things after a file changed (swp, signcolumn,
" ...), default: 4000 ms
set updatetime=100

" Show a few lines of context around the cursor. Note that this makes the
" text scroll if you mouse-click near the start or end of the window.
set scrolloff=5

" Do incremental searching.
set incsearch

" Don't use Ex mode, use Q for formatting.
map Q gq

" Ignore case unless the earch phrase contains an upper case character
set ignorecase
set smartcase

" display relative numbers
set number relativenumber

" soft wrap complete words
set linebreak

set matchpairs+=<:>

" Hide syntax e.g. in markdown
set conceallevel=1

" Open split panes to the right and below instead of left and above
set splitbelow
set splitright

" Quicker window navigation
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l
